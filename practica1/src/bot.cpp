#include <stdio.h>

#include "DatosMemCompartida.h"
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>      
#include <stdlib.h>
#include <unistd.h>


int main(int argc, char const *argv[])
{

	int f;
	char *map;
	DatosMemCompartida *datos;

	f=open("/tmp/datos.txt",O_RDWR,0777);
	map=(char*)mmap(NULL,sizeof(*(datos)),PROT_WRITE|PROT_READ,MAP_SHARED,f,0);
	close(f);
	datos=(DatosMemCompartida*)map;
	

	float posicion;

	while(1)
	{
		posicion = (datos->raqueta1.y2+datos->raqueta1.y1)/2;//posicion=centro raqueta
		if(posicion<datos->esfera.centro.y)
		{
			datos->accion=1;
		}
		else if(posicion>datos->esfera.centro.y)
		{
			datos->accion=-1;
		}
		else
		{
			datos->accion=0;
		}
		usleep(25000);
	}
	
	munmap(map,sizeof(*(datos)));
	return 0;
}
